using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] float raleOffire;

    [SerializeField] Projectile projectile;

    [HideInInspector]
    private GameObject[] gameManager;

    private GameObject gameObj;

    private WeaponRelorder relorder;

    float nextFireAllowed;
    public bool canFire;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectsWithTag("Muzzle");
        gameObj = gameManager[0];
        relorder = GetComponent<WeaponRelorder>();
    }

    public void Reload()
    {
        if (relorder == null)
        {
            return;
        }
        relorder.Reload();
    }

    public virtual void Fire()
    {
        canFire = false;
        if (Time.time < nextFireAllowed)
        {
            return;
        }

        if (relorder != null)
        {
            if (relorder.IsReloding)
            {
                return;
            }

            if (relorder.RoundsRemaIningClip == 0)
            {
                return;
            }
            relorder.TakeFromClip(1);
        }

        nextFireAllowed = Time.time + raleOffire;
        float x = gameObj.transform.position.x ;
        float y = gameObj.transform.position.y ;
        float z = gameObj.transform.position.z ;
        Vector3 spawnPos = new Vector3(x, y, z);
        //Instantiate(projectile, spawnPos, gameObj.transform.rotation);
        Instantiate(projectile, spawnPos, Quaternion.identity);
        canFire = true;
    }
}
