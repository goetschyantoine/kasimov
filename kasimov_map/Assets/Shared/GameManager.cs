using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager
{
    public event System.Action<Player> OnLocalPlayerJoined;
    
    private GameObject gameObject; 
    private static GameManager m_Instance;

    private GameObject[] gameManager;

    private GameObject gameObj;

    private InputController m_InputController;
    public static GameManager Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new GameManager();
                m_Instance.gameObject = new GameObject("_gameManager");
                m_Instance.gameObject.AddComponent<InputController>();
                m_Instance.gameObject.AddComponent<Timer>();
                m_Instance.gameObject.AddComponent<Respawn>();
            }
            return m_Instance;
        }
    }

    

    public InputController InputController
    {
        get
        {
            if (m_InputController == null)
            {
                m_InputController = gameObject.GetComponent<InputController>();
            }
            return m_InputController;
        }
    }

    private Timer m_Timer;

    public Timer Timer
    {
        get
        {
            if (m_Timer == null)
            {
                m_Timer = gameObject.GetComponent<Timer>();
            }

            return m_Timer;
        }
    }

    private Respawn m_Respawn;

    public Respawn Respawn
    {
        get
        {
            if (m_Respawn == null)
            {
                m_Respawn = gameObject.GetComponent<Respawn>();
            }

            return m_Respawn;
        }
    }
    
    
    private Player m_LocalPlayer;
    public Player LocalPlayer
    {
        get
        {
            return m_LocalPlayer;
        }
        set
        {
            m_LocalPlayer = value;
            if (OnLocalPlayerJoined != null)
            {
                OnLocalPlayerJoined(m_LocalPlayer);
            }
        }
    }

}
