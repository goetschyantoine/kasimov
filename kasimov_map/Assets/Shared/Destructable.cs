using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructable : MonoBehaviour
{
    [SerializeField]float hitPoints;

    public event System.Action OnDeath;
    public event System.Action OnDamageReceveid;

    float damageTaken;

    public float HitPointsRemaing
    {
        get
        {
            return hitPoints - damageTaken;
        }
    }

    public bool IsAlive
    {
        get
        {
            return HitPointsRemaing > 0;
        }
    }

    public virtual void Die()
    {
        if (!IsAlive)
        {
            return;
        }

        if (OnDeath != null)
        {
            OnDeath();
        }
    }

    public virtual void TakeDamage(float amount)
    {
        damageTaken += amount;
        if(OnDamageReceveid != null)
        {
            OnDamageReceveid();
        }

        if (HitPointsRemaing <= 0)
        {
            Die();
        }
    }

    public void Reset()
    {
        damageTaken = 0; 
    }
}
