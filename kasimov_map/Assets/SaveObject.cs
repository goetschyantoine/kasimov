using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveObject : MonoBehaviour
{
    public GameObject obj;

    private void Awake()
    {
        DontDestroyOnLoad(obj);
    }
}
