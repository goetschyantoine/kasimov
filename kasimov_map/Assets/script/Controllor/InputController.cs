using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public float Vertical; //Constructs Vertical float public number
    public float Horizontal; //Constructs Horizontal float public number
    public Vector2 MouseInput; //Constructs MousseInput Vector2 public number
    public bool Fire1;
    public bool Reload;
    public bool Walking;
    public bool Running;
    public bool Sprinting;
    public bool Crounching;
    public string[] CommandsButton = { "Fire1" };
    public List<KeyCode> CommandsKey = new List<KeyCode> ();

    void Awake()
    {
        CommandsKey.Add (KeyCode.Mouse0);
        CommandsKey.Add(KeyCode.R);
        CommandsKey.Add(KeyCode.LeftAlt);
        CommandsKey.Add(KeyCode.LeftShift);
        CommandsKey.Add(KeyCode.C);
        CommandsKey.Add(KeyCode.O);
    }

    public KeyCode GetList(int index)
    {
        return CommandsKey[index];
    }
    public void ModifTouchButton(string touch, int index)
    {
        CommandsButton[index] = touch;
    }
    public void ModifTouchKeys(KeyCode touch,int index)
    {
        CommandsKey[index] = touch;
    }

    private void Update()
    {
        if (!Main.GetMinimenu())
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Vertical = Input.GetAxis("Vertical"); //Vertical = Vertical coordinate
            Horizontal = Input.GetAxis("Horizontal"); //Horizontal = Horizontal coordinate
            MouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")); //MousseInput = give Mouse X and Mouse Y coordinate

            Fire1 = Input.GetKey(CommandsKey[0]);
            Reload = Input.GetKey(CommandsKey[1]);
            Walking = Input.GetKey(CommandsKey[2]);
            Running = Input.GetKey(CommandsKey[3]);
            Sprinting = Input.GetKey(CommandsKey[4]);
            Crounching = Input.GetKey(CommandsKey[5]);
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
