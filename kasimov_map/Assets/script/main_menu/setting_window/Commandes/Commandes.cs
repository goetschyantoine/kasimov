using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Commandes : MonoBehaviour
{

    private GameObject[] gameManager;

    private GameObject gameObj;

    private InputController controller;
    public Text text;
    public int index;
    bool x = false;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectsWithTag("GameManager");
        gameObj = gameManager[0];
        controller = Instantiate(gameObj).GetComponent<InputController>();
        ChangeText();
    }
    public void ChangeInputKey()
    {
        x = true;
    }

    public void ChangeText()
    {
        text.text = controller.GetList(index).ToString();
    }

    private void Update()
    {
          if (x)
            {
                foreach (KeyCode elt in Enum.GetValues(typeof(KeyCode)))
                {
                    if (Input.GetKey(elt))
                    {
                        controller.ModifTouchKeys(elt, index);
                        x = false;
                        ChangeText();
                    }

                }
            }
        
        
        
    }

}
