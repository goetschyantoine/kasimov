using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class SettingsScript : MonoBehaviour
{

    Resolution[] resolution;

    public Dropdown ResDropdown;

    public GameObject setWind;
    public GameObject setWind2;
    public GameObject setWind3;

    // Start is called before the first frame update
    void Start()
    {
        resolution = Screen.resolutions.Select(resolutions => new Resolution { width=resolutions.width,height=resolutions.height}).Distinct().ToArray();
        ResDropdown.ClearOptions();

        List<string> option = new List<string>();

        int currentres=0;

        for (int i = 0; i < resolution.Length; i++)
        {
            string opt = resolution[i].width + "x" + resolution[i].height;
            option.Add(opt);
            if (resolution[i].width == Screen.width && resolution[i].height== Screen.height)
            {
                currentres = i;
            }
        }

        ResDropdown.AddOptions(option);
        ResDropdown.value = currentres;
        ResDropdown.RefreshShownValue();
    }

    public void SetResolution(int ResIndex)
    {
        Resolution resol = resolution[ResIndex];
        Screen.SetResolution(resol.width,resol.height,true);
    }

    public void Global()
    {
        setWind.SetActive(true);
        setWind2.SetActive(false);
        setWind3.SetActive(false);
    }

    public void Commandes()
    {
        setWind.SetActive(false);
        setWind2.SetActive(true);
        setWind3.SetActive(false);
    }

    public void Autres()
    {
        setWind.SetActive(false);
        setWind2.SetActive(false);
        setWind3.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
