using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MiniMenu : MonoBehaviour
{

    public float Vertical; //Constructs Vertical float public number
    public float Horizontal; //Constructs Horizontal float public number


    public GameObject setWind;
    public GameObject setWind2;

    public Main SetSettings;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    public static void ActiveScene()
    {
        SceneManager.GetActiveScene();
    }
    public void ExitSettings()
    {
        SetSettings.Settings = false;
        setWind.SetActive(false);
        setWind2.SetActive(true);
    }

    public void Settings()
    {
        SetSettings.Settings = true;
        setWind.SetActive(true);
        setWind2.SetActive(false);
    }

    public void Quit()
    {
        SceneManager.UnloadSceneAsync("map_kasimov");
        SceneManager.LoadScene("Menu_Kasimov");
       
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
