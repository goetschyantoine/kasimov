using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuKasimov : MonoBehaviour
{
    public string sceneName;

    public GameObject setWind;
    public GameObject setWind2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void StartGame()
    {
        SceneManager.LoadScene(sceneName);
    }


    public void Settings()
    {
        setWind.SetActive(false);
        setWind2.SetActive(true);
    }

    public void Menu()
    {
        setWind.SetActive(true);
        setWind2.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
