using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{

    public GameObject setWind;
    public static bool Minimenu = false;
    // Start is called before the first frame update
    private Player player;
    public bool Settings=false;
    void Start()
    {
        
    }

    public static bool GetMinimenu()
    { 
        return Minimenu;
    }

    public void ChangeMenu()
    {
        if (Minimenu)
        {
            setWind.SetActive(true);
        }
        else
        {
            setWind.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Settings)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (Minimenu)
                {
                    resume();
                }
                else
                {
                    paused();
                }
            }
        }
    }

    public void paused()
    {
        Minimenu = true;
        ChangeMenu();
    }

    public void resume()
    {
        Minimenu = false;
        ChangeMenu();
    }
}
