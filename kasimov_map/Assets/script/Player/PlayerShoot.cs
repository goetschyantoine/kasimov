using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    private GameObject[] gameManager;

    private GameObject gameObj;

    private InputController controller;

    [SerializeField]Shooter assaultRaffle;

    private void Awake()
    {
        gameManager= GameObject.FindGameObjectsWithTag("GameManager");
        gameObj =gameManager[0];
        controller = Instantiate(gameObj).GetComponent<InputController>(); 
    }
    private void Update()
    {
        if (controller.Fire1)
        {
            assaultRaffle.Fire();
        }
    }
}
