using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MoveController))]
public class Player : MonoBehaviour
{
   [System.Serializable] //Initialisation of Serializable
    public class MouseInput
    {
        public Vector2 Damping; //Constructs Damping with a new vector with given x, y components.
        public Vector2 Sensitivity; //Constructs Sensitivity with a new vector with given x, y components.
        public bool LockMouse;
    }
    
    [SerializeField] float runspeed; //Constructs speed are float 
    [SerializeField]float walkspeed;
    [SerializeField]float sprintSpeed;
    [SerializeField]float crounchSpeed;
    [SerializeField] MouseInput MouseControl;


    
    
    private MoveController m_MoveController;
    public MoveController MoveController
    {
        get
        {
            if (m_MoveController == null)
            {
                m_MoveController = GetComponent<MoveController>();
            }
            return m_MoveController;
        }
    }

    private Crosshair m_Crosshair;

    private Crosshair Crosshair
    {
        get
        {
            if (m_Crosshair == null)
            {
                m_Crosshair = GetComponentInChildren<Crosshair>();
            }

            return m_Crosshair;
        }
    }

    private GameObject[] gameManager;

    private GameObject gameObj;

    private InputController playerInput;

    Vector2 mouseInput;
    
    void Awake()
    {
        gameManager = GameObject.FindGameObjectsWithTag("GameManager");
        gameObj = gameManager[0];
        playerInput= Instantiate(gameObj).GetComponent<InputController>();
        GameManager.Instance.LocalPlayer = this;
        if (MouseControl.LockMouse)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        LookAround();}

    void Move()
    {
        float moveSpeed = runspeed;
        
        if (playerInput.Walking)
        {
            moveSpeed = walkspeed;
        }
        if (playerInput.Sprinting)
        {
            moveSpeed = sprintSpeed;
        }

        if (playerInput.Crounching)
        {
            moveSpeed = crounchSpeed;
        }
        
        Vector2 direction = new Vector2(playerInput.Vertical * moveSpeed,playerInput.Horizontal * moveSpeed);
        MoveController.Move(direction);
    }

    void LookAround()
    {
        mouseInput.x = Mathf.Lerp(mouseInput.x, playerInput.MouseInput.x ,1f / MouseControl.Damping.x);
        mouseInput.y = Mathf.Lerp(mouseInput.y, playerInput.MouseInput.y ,1f / MouseControl.Damping.y);
        transform.Rotate(Vector3.up * mouseInput.x * MouseControl.Sensitivity.x);
        Crosshair.Lookheight(mouseInput.y * MouseControl.Sensitivity.y);
    }
}
