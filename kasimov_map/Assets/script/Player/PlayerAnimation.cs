using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public Animator animator;

    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        animator.SetFloat("Vertical", GameManager.Instance.InputController.Vertical);
        animator.SetFloat("Horizontal",GameManager.Instance.InputController.Horizontal);
        animator.SetBool("Walking",GameManager.Instance.InputController.Walking);
        animator.SetBool("Sprinting",GameManager.Instance.InputController.Sprinting);
        animator.SetBool("Crounching",GameManager.Instance.InputController.Crounching);



    }
}
