using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponRelorder : MonoBehaviour
{
    [SerializeField]int maxAmmo;
    [SerializeField]float reloadTime;
    [SerializeField]int clipSize;

    int ammo;
    public int shotFireIntClip;
    bool isReloading;

    public int RoundsRemaIningClip
    {
        get
        {
            return clipSize - shotFireIntClip;
        }
    }

    public bool IsReloding
    {
        get
        {
            return isReloading;
        }
    }

    public void Reload()
    {
        if (isReloading)
        {
            return;
        }
        

        isReloading = true;
        print("Reload star");
        GameManager.Instance.Timer.Add(ExecuteReload,reloadTime);
    }

    private void ExecuteReload()
    {
        print("Reload execute");
        isReloading = false;
        ammo -= shotFireIntClip;
        shotFireIntClip = 0;

        if (ammo < 0)
        {
            ammo = 0;
            shotFireIntClip += -ammo;
        }
        
    }

    public void TakeFromClip(int amount)
    {
        shotFireIntClip += amount;
        
    }
}
