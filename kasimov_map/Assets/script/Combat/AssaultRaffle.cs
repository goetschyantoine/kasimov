using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssaultRaffle : Shooter
{
    private GameObject[] gameManager;

    private GameObject gameObj;

    private InputController controller;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectsWithTag("GameManager");
        gameObj = gameManager[0];
        controller = Instantiate(gameObj).GetComponent<InputController>();
    }
    public override void Fire()
    {
        base.Fire();
        if (canFire)
        {
            //we fire the gun
        }
        
    }

    public void Update()
    {
        if (controller.Reload)
        {
            Reload();
        }
    }
}
