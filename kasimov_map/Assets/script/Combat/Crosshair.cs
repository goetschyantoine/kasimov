using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    [SerializeField]Texture2D image;
    [SerializeField]int size;
    [SerializeField]float maxAngle;
    [SerializeField]float minAngle;

    float lookheight;

    public void Lookheight(float value)
    {
        lookheight += value;
        if (lookheight > maxAngle || lookheight < maxAngle)
        {
            lookheight -= value;
        }
        void OnGui()
        {
            Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
            screenPosition.y = Screen.height - screenPosition.y;
            GUI.DrawTexture(new Rect(screenPosition.x,screenPosition.y - lookheight,size,size),image);
        }
    }
    
}
