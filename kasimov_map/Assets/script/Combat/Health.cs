using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : Destructable
{
    [SerializeField]float inSeconds;
    public override void Die()
    {
        base.Die();
        GameManager.Instance.Respawn.Despawn(gameObject,inSeconds);
    }

    private void OnEnable()
    {
        Reset();
    }

    public override void TakeDamage(float amount)
    {
        base.TakeDamage(amount);
        print("Remaining" + HitPointsRemaing);
    }
}
